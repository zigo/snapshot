# vim: set ts=2 sw=2 et si sm:

WSGIDaemonProcess snapshot.debian.org user=nobody group=nogroup home=/ processes=4 threads=10 maximum-requests=5000 inactivity-timeout=1800 umask=0077 display-name=wsgi-snapshot.debian.org

<VirtualHost *:80>
  ServerName snapshot.debian.org
  ServerAlias snapshot-dev.debian.org snapshot-master.debian.org snapshot-*.debian.org snapshot.debian.net
  ServerAdmin team@snapshot.debian.org

  ErrorLog  /var/log/apache2/snapshot.debian.org-error.log
  CustomLog /var/log/apache2/snapshot.debian.org-access.log combined

  Alias /static      /srv/snapshot.debian.org/web/public/static
  Alias /robots.txt  /srv/snapshot.debian.org/web/public/robots.txt

  #Alias /git         /srv/snapshot.debian.org/snapshot.git
  SetEnv GIT_PROJECT_ROOT /srv/snapshot.debian.org/all-git
  AliasMatch ^/gits/(.*)$ /srv/snapshot.debian.org/all-git/$1

  RewriteEngine on

  # forbid POST requests
  RewriteCond %{REQUEST_METHOD} POST
  RewriteRule .* - [F,L]

  RewriteRule ^/archive/backports.org/(.*) /archive/debian-backports/$1 [L,R]
  RewriteRule ^/git/(.*) /gits/snapshot.git/$1 [PT]

  RewriteCond  %{HTTP_HOST} ^snapshot.debian.net$
  RewriteRule ^/(.*)$ http://snapshot.debian.org/ [R]

  <Location /gits/>
    Require all granted
  </Location>
  <Directory /srv/snapshot.debian.org/web/public>
    Require all granted
  </Directory>
  <Directory /srv/snapshot.debian.org/bin>
    <Files snapshot.wsgi>
      Require all granted
    </Files>
  </Directory>

  AliasMatch "^/file/([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{36})$" "/srv/snapshot.debian.org/farm/$1/$2/$1$2$3"
  <Directory /srv/snapshot.debian.org/farm>
    Require all granted
    Header set Cache-Control "max-age=31536000, public"
  </Directory>

  WSGIScriptAlias / /srv/snapshot.debian.org/bin/snapshot.wsgi
  WSGIProcessGroup snapshot.debian.org
  #WSGIPassAuthorization On
</VirtualHost>

<VirtualHost *:80>
  ServerName snapshots.debian.org
  RedirectPermanent / http://snapshot.debian.org/
</VirtualHost>
